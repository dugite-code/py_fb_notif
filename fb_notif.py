import json, requests, pickle, io, ConfigParser, datetime, csv, sqlite3
from pushbullet import Pushbullet

requests.packages.urllib3.disable_warnings()

try:	
	#Config file read
	config = ConfigParser.ConfigParser()
	config.read("pref.conf")
	
	#Load config Variables
	fb_token=config.get("py_fb_notif", "Facebook_Token")
	pb_token=config.get("py_fb_notif", "Push_bullet_API_Key")
	
except:
	ts = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
	print (ts, "Critical Fail: unable to load/read pref.conf")
	exit()
	
pb = Pushbullet(pb_token)

try:
	with open('Mworkfile', 'rb') as f:
		message_store = pickle.load(f)
except:
	print("unable to open the message store")
	message_store =[]
	
try:
	with open('Nworkfile', 'rb') as f:
		notif_store = pickle.load(f)
except:
	print("unable to open the notification store")
	notif_store =[]

fb_api = "https://graph.facebook.com/v2.2/"
fb_api_req = "me/notifications?access_token="

url = fb_api + fb_api_req + fb_token
notif = requests.get(url).json()

lcount = 0
ncount = 0

for i in notif["data"]:
	old = "no"
	for j in notif_store:
		if notif["data"][lcount]["id"] == j:
			old = "yes"
			break
			
	if old != "yes":
		ncount += 1
		notifid = notif["data"][lcount]["id"]
		notif_store.extend([notifid])
		lcount += 1
	
[str(x) for x in notif_store]

print ncount
if ncount > 0:
	title = str(ncount) + " new Facebook notification"
	if ncount >1:
		title = title + "s"
	print title
	push = pb.push_link(title, "https://m.facebook.com/notifications")

with io.open('Nworkfile', 'wb') as f:
    pickle.dump(notif_store, f)	
	
fb_api_req = "me/inbox?fields=unread&access_token="

url = fb_api + fb_api_req + fb_token
message = requests.get(url).json()

lcount = 0
mcount = 0
for i in message["data"]:
	if message["data"][lcount]["unread"] == 0:
		old = "no"
		for j in message_store:
			if message["data"][lcount]["updated_time"] == j:
				old = "yes"
				break
			
		if old != "yes":
			mcount += 1
			u_time = message["data"][lcount]["updated_time"]
			message_store.extend([u_time])
			
	lcount += 1

[str(x) for x in message_store]
print(message_store)

sqlite_file = "./sqlite_file.db"
table_name1 = "notif_store"
table_name2 = "message_store"
new_field = "message_u_time"
field_type = "VARCHAR(255)"

# Connecting to the database file
conn = sqlite3.connect(sqlite_file)
c = conn.cursor()

# Creating a new SQLite table with 1 column
c.execute('CREATE TABLE IF NOT EXISTS {tn} ({nf} {ft})'\
        .format(tn=table_name2, nf=new_field, ft=field_type))

for item in message_store:
	item.encode('utf-8')
	item = (item,)
	print(item)
	c.execute("INSERT INTO " + table_name2 + " (" + new_field + ") VALUES (?)", item)
conn.commit()

c.execute('select * from ' + table_name2)
print (c.fetchall())

print (mcount)
if mcount > 0:
	title = str(mcount) + " new Facebook message"
	if mcount >1:
		title = title + "s"
	print title
	push = pb.push_link(title, "https://m.facebook.com/messages/")

with io.open('Mworkfile', 'wb') as f:
    pickle.dump(message_store, f)