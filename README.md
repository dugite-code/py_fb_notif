# py_fb_notif
Facebook notifications through pushbullet writen in python

## Requirements
  * Python 2.7x - json, requests, pickle, io, ConfigParser, datetime, csv, sqlite3, [pushbullet](https://github.com/randomchars/pushbullet.py)
  * [Pushbullet API key](https://api.pushbullet.com/)
  * [Facebook long term access key](https://stackoverflow.com/questions/12168452/long-lasting-fb-access-token-for-server-to-pull-fb-page-info/21927690#21927690)
